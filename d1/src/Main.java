public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");

        Car car1 = new Car();

        //car1.make = "Veyron";
        //car1.brand = "Bugatti";
        //car1.price = 2000000;
        //System.out.println(car1.brand);
        //System.out.println(car1.make);
        //System.out.println(car1.price);

        //Instance - an object created from a class and each instance should be independent from one another
        Car car2 = new Car();
        //car2.make = "Tamaraw FX";
        //car2.brand = "Toyota";
        //car2.price = 450000;
        //System.out.println(car2.brand);
        //System.out.println(car2.make);
        //System.out.println(car2.price);

        //We cannot add value to a property that is not in our class

        Car car3 = new Car();
        //car3.make = "Mini Cooper";
        //car3.brand = "Mini";
        //car3.price = 2650000;
        //System.out.println(car3.brand);
        //System.out.println(car3.make);
        //System.out.println(car3.price);

        Car car4 = new Car();
        //System.out.println(car4.brand);

        Driver driver1 = new Driver("Alejandro",25);
        Car car5 = new Car("Vios","Toyota",1500000,driver1);
        //System.out.println(car5.make);
        //System.out.println(car5.brand);
        //System.out.println(car5.price);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        //make property getters
        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        //make property setters
        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car5.setMake("Innova");
        System.out.println(car5.getMake());

        car1.setBrand("Bugatti");
        car1.setPrice(2000000);
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        System.out.println(car5.getCarDiver().getName());

        Driver newDriver = new Driver("Antonio",21);
        car5.setCarDiver(newDriver);

        //get name of new carDriver
        System.out.println(car5.getCarDiver().getName());
        System.out.println(car5.getCarDiver().getAge());

        //Custom getCarDriverName method
        System.out.println(car5.getCarDriverName());


        Animal pet1 = new Animal("Chicken Little","Yellow");
        pet1.call();
    }
}