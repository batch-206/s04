public class Car {

    // Properties/Attributes - the charaacteristics of the object the class will create

    //Constructor - method to create the object and instantiate with its initialized values

    //Getters and Setters - are methods to get values of an object's properties or set them

    //Methods - actions that an object can perform or do.

    //public access - tha variable/property in the class is accessible anywhere in the application
    //Attributes/properties of a class should not be made public. They should only be accessed with getters and setters instead of just dot notation.
    //private - limits the access and ability to get or set a variable/method to only within its own class
    //getters - methods that returns the value of the property
    //setters - methods that allow us to set the value of a property

    private String make;

    private String brand;

    private int price;

    private Driver carDiver;

    //Constructor
    //empty/default constructor - allows us to create an instance with default initialized values
    //By default, when your class does not have a constructor, Java will assign one for you. Java also set the default values. You could have a way to add your own default values.

    //Default Constructor
    public Car(){
        //this.brand = "Geely";
        this.carDiver = new Driver();
    }

    //Parameterized
    public Car(String make,String brand,int price, Driver driver){
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDiver = driver;
    }

    //Getters and Setters for our properties
    //Getters return a value therefore we must add a dataType of the value returned

    //You could set a property as read-only. This means that the value of the property can only be get but not updated

    public String getMake(){
        //this keyword also refers to the object/instance
        return this.make;
    }

    public void setMake(String makeParams){
        this.make = makeParams;
    }

    public String getBrand(){
        return this.brand;
    }

    public void setBrand(String brandParams){
        this.brand = brandParams;
    }

    public Integer getPrice(){
        return this.price;
    }

    public void setPrice(Integer priceParams){
        this.price = priceParams;
    }

    //Methods
    //void - means that the function does not return anything. Because in Java, methods return dataType must also be declared
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

    public Driver getCarDiver() {
        return carDiver;
    }

    public void setCarDiver(Driver carDiver) {
        this.carDiver = carDiver;
    }

    //custom method to retrieve the car driver's name:
    public String getCarDriverName(){
        return this.carDiver.getName();
    }
}
